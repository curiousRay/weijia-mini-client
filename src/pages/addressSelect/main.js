import Vue from 'vue'
import App from './AddressSelect'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '选择收货地址'
  }
}
