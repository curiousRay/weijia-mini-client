import Vue from 'vue'
import App from './AddressManager'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '地址管理',
    enablePullDownRefresh: true,
    backgroundTextStyle: 'light'
  }
}
