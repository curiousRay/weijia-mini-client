import Vue from 'vue'
import App from './DiyPage'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: 'DIY',
    enablePullDownRefresh: true
  }
}
