import Vue from 'vue'
import App from './FavoritesPage'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '我的收藏',
    enablePullDownRefresh: true
  }
}
