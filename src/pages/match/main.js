import Vue from 'vue'
import App from './Match'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '比赛详情'
  }
}
