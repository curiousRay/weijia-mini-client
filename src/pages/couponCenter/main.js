import Vue from 'vue'
import App from './CouponCenter'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '领劵中心'
  }
}
