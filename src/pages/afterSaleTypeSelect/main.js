import Vue from 'vue'
import App from './AfterSaleTypeSelect'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '申请售后'
  }
}
