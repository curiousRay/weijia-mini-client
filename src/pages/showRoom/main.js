import Vue from 'vue'
import App from './ShowRoomPage'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '样板间'
  }
}
