import Vue from 'vue'
import App from './DiyDetail'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: 'DIY'
  }
}
