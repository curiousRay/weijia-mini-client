import Vue from 'vue'
import App from './OrderPaySuccess'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '订单支付成功'
  }
}
