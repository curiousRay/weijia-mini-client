import Vue from 'vue'
import App from './Evaluate'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '评价'
  }
}
