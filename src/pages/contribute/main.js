import Vue from 'vue'
import App from './Contribute'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '投稿'
  }
}
