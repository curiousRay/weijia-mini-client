import Vue from 'vue'
import App from './GoodsDetail'

new Vue(App).$mount()

export default {
  config: {
    navigationBarTitleText: '商品详情'
  }
}
