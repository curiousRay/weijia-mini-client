import Vue from 'vue'
import App from './StoreList'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '身边门店'
  }
}
