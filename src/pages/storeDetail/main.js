import Vue from 'vue'
import App from './StoreDetail'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: ' '
  }
}
