import Vue from 'vue'
import App from './StoreCoupon'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '本店福利'
  }
}
