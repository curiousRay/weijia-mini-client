import Vue from 'vue'
import App from './VR'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: 'VR'
  }
}
