import Vue from 'vue'
import App from './Discover'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '发现',
    enablePullDownRefresh: true
  }
}
