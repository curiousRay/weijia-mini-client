import Vue from 'vue'
import App from './AllPeopleDesignPage'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '全民设计',
    enablePullDownRefresh: true
  }
}
