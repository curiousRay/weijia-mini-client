import Vue from 'vue'
import App from './Summary'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '评价详情'
  }
}
