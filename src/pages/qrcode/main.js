import Vue from 'vue'
import App from './Qrcode'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTextStyle: 'white',
    navigationBarBackgroundColor: '#BE885F',
    backgroundColor: '#BE885F',
    backgroundColorTop: '#BE885F',
    backgroundColorBottom: '#D09D7A',
    navigationBarTitleText: '二维码'
  }
}
