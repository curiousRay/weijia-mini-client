import Vue from 'vue'
import App from './Express'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '快递信息'
  }
}
