import Vue from 'vue'
import App from './StoreClass'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '分类好物'
  }
}
