import Vue from 'vue'
import App from './UserCenterPage'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarBackgroundColor: '#CB9871',
    navigationBarTitleText: ''
  }
}
