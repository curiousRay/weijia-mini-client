import Vue from 'vue'
import App from './MyContribute'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '我的投稿'
  }
}
