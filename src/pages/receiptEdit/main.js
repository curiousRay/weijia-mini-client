import Vue from 'vue'
import App from './ReceiptEdit'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '发票'
  }
}
