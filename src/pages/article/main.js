import Vue from 'vue'
import App from './Article'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: ''
  }
}
