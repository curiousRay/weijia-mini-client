import Vue from 'vue'
import App from './ClassPage'

const app = new Vue(App)
app.$mount()

export default {
  config: {
    navigationBarTitleText: '分类'
  }
}
