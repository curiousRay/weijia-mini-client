import api from './api'
import { setToken } from './utils/token'
import navigate from './utils/navigate'

export default function login ($query) {
  return new Promise((resolve, reject) => {
    wx.login({
      success: async res => {
        const { token } = await api.customer.login({
          code: res.code
        })

        setToken(token)

        wx.getSetting({
          success: res => {
            if (!res.authSetting['scope.userInfo']) {
              navigate.to('/pages/login/main', $query)
            } else {
              wx.getUserInfo({
                success ({ encryptedData, iv }) {
                  api.customer.update({
                    encryptedData,
                    iv
                  })

                  resolve()
                },
                error (e) {
                  reject(e)
                  navigate.to('/pages/login/main', $query)
                }
              })
            }
          }
        })
      }
    })
  })
}
