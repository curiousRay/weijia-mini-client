import store from '../../utils/store'

function save (str) {
  let searchHistory = get()

  if (!str || searchHistory.includes(str)) {
    return
  }

  searchHistory.push(str)
  store.set('searchHistory', searchHistory)
}

function get () {
  return store.get('searchHistory') || []
}

function clear () {
  store.remove('searchHistory')
}

export default {
  save,
  get,
  clear
}
