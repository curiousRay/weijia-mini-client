import api from '../api'

export default {
  namespaced: true,
  state: {
    flagshipStore: {}
  },
  actions: {
    getFlagshipStore ({ commit, state }) {
      const storeCode = state.flagshipStore.code

      if (storeCode) {
        return new Promise(resolve => resolve(storeCode))
      } else {
        return api.store.flagshipStore().then(res => {
          commit('update', {
            flagshipStore: res
          })
        })
      }
    }
  },
  mutations: {
    update (state, data) {
      Object.assign(state, data)
    }
  }
}
