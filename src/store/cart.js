import navigate from '../utils/navigate'
import store from '../utils/store'

export default {
  namespaced: true,
  state: {
    goodsList: store.get('goodsList') || [],
    buyInfo: {}
  },
  actions: {
  },
  mutations: {
    update (state, data) {
      Object.assign(state, data)
    },
    buy (state, buyInfo) {
      state.buyInfo = buyInfo

      navigate.to('/pages/orderConfirm/main')
    },
    set (state, { skuItem, value }) {
      const list = state.goodsList.find(item => item.storeCode === skuItem.storeCode)

      if (list) {
        const findedSku = list.skuList.find(item => {
          return item.sku.id === skuItem.sku.id
        })

        Object.assign(findedSku, value)

        // remove goods from skulist
        if (value.amount === 0) {
          const index = list.skuList.findIndex(item => {
            return item.sku.id === skuItem.sku.id
          })

          list.skuList.splice(index, 1)
        }
      }

      store.set('goodsList', state.goodsList)
    },
    selectAll (state, storeCode) {
      if (!storeCode) {
        return
      }

      state.goodsList
        .find(item => item.storeCode === storeCode)
        .skuList
        .forEach(item => {
          item.selected = true
        })
    },
    notSelectAll (state, storeCode) {
      if (!storeCode) {
        return
      }

      state.goodsList
        .find(item => item.storeCode === storeCode)
        .skuList
        .forEach(item => {
          item.selected = false
        })
    },
    add (state, skuItem) {
      const list = state.goodsList.find(item => item.storeCode === skuItem.storeCode)

      skuItem.selected = false

      // check list
      if (list) {
        const findedSku = list.skuList.find(item => {
          return item.sku.id === skuItem.sku.id
        })

        // check sku
        if (findedSku) {
          findedSku.amount += skuItem.amount
        } else {
          list.skuList.push(skuItem)
        }
      } else {
        state.goodsList.push({
          skuList: [skuItem],
          storeCode: skuItem.storeCode
        })
      }

      store.set('goodsList', state.goodsList)
    }
  },
  getter: {
    storeCart (state, storeCode) {
      return state.goodsList.find(item => item.storeCode === storeCode)
    }
  }
}
