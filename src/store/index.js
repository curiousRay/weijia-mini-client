import Vue from 'vue'
import Vuex from 'vuex'

import user from './user'
import global from './global'
import cart from './cart'
import orderConfirm from './orderConfirm'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    user,
    global,
    cart,
    orderConfirm
  }
})

export default store
