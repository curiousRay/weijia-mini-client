import navigate from '../utils/navigate'

export default {
  namespaced: true,
  state: {
    address: {},
    coupon: {},
    receipt: {},
    buyInfo: {},
    mainOrder: {}
  },
  actions: {
    init () {
    }
  },
  mutations: {
    update (state, data) {
      Object.assign(state, data)
    },
    buy (state, buyInfo) {
      state.buyInfo = buyInfo

      navigate.to('/pages/orderConfirm/main')
    }
  }
}
