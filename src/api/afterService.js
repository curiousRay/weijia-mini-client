import { get, put, post } from '../utils/request'

export default {
  all: data => get(`/wx/afterService/list`, data),
  query: data => get(`/wx/afterService/list`, data),
  get: code => get(`/wx/afterService/${code}`),
  courier: data => put(
    `/wx/afterService/courier?serviceCode=${data.serviceCode}&expressCompanyName=${data.expressCompanyName}&expressCompanyCode=${data.expressCompanyCode}&courierNumber=${data.courierNumber}`
  ),
  post: data => post(
    `/wx/afterService/add?orderCode=${data.orderCode}&serviceType=${data.serviceType}&reason=${data.reason}&explain=${data.explain}&picUrls=${data.picUrls}`
  )
}
