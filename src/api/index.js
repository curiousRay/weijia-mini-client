import search from './search'
import customer from './customer'
import logistics from './logistics'
import store from './store'
import commodity from './commodity'
import banner from './banner'
import evaluation from './evaluation'
import discovery from './discovery'
import coupon from './coupon'
import invoice from './invoice'
import order from './order'
import delivery from './delivery'
import template from './template'
import collection from './collection'
import designmatch from './designmatch'
import matchworks from './matchworks'
import policy from './policy'
import diy from './diy'
import afterService from './afterService'

const api = {
  search,
  customer,
  logistics,
  store,
  commodity,
  banner,
  evaluation,
  discovery,
  coupon,
  invoice,
  order,
  delivery,
  template,
  collection,
  designmatch,
  matchworks,
  policy,
  diy,
  afterService
}

export default api
