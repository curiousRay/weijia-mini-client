
import {get, post, put, del} from '../utils/request'

export default {
  get: () => get('/wx/delivery/customer'),
  add: data => post('/wx/delivery/', data),
  update: data => put('/wx/delivery/', data),
  delete: data => del('/wx/delivery/' + data.id, data),
  default: () => get('/wx/delivery/default')
}
