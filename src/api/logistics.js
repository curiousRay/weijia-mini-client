import { get } from '../utils/request'

export default {
  query: ({ comid, number }) => get(`/wx/logistics/${comid}/${number}/`),
  company: () => get('/wx/logistics/company/')
}
