import { get, post } from '../utils/request'

export default {
  all: data => get(`/wx/collection/`, data),
  count: () => get(`/wx/collection/count/`),
  fav: data => post(`/wx/collection/discovery/`, data)
}
