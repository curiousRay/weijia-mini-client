import { get, post } from '../utils/request'

export default {
  get: id => get(`/wx/works/${id}`),
  getUserContribution: data => get(`/wx/works/customer`, data),
  vote: id => post(`/wx/works/${id}/vote`),
  addWork: data => post(`/wx/works/`, data)
}
