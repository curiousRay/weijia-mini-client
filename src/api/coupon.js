import { get, post } from '../utils/request'

export default {
  store: storeCode => get(`/wx/coupon/store/${storeCode}`),

  // 领取优惠券
  customer: id => post(`/wx/coupon/${id}/customer`),

  // 我的优惠券
  my: couponStatus => get(`/wx/coupon/customer/status/${couponStatus}`),
  order: data => post(`/wx/coupon/order`, data)
}
