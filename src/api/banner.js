import { get } from '../utils/request'

export default {
  get: storeCode => get(`/wx/banner/${storeCode}`)
}
