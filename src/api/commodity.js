import { get } from '../utils/request'

export default {
  search: data => get(`/wx/commodity/${data.storeCode}/search`, data),
  recommand: storeCode => get(`/wx/commodity/${storeCode}/recommend`),
  detail: ({ storeCode, commodityCode }) => get(`/wx/commodity/${storeCode}/${commodityCode}/detail`),
  sku: data => get(`/wx/commodity/sku`, data)
}
