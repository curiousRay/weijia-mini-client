import { post, get } from '../utils/request'

export default {
  getAll: data => get(`/wx/template/`, data),
  get: id => get(`/wx/template/${id}`),
  commodity: id => get(`/wx/template/${id}/commodity`),
  comment: (templateCode, data) => get(`/wx/template/${templateCode}/comment`, data),
  addComment: data => post(`/wx/template/comment`, data),
  like: data => post(`/wx/template/comment/like/`, data)
}
