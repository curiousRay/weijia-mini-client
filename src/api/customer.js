import { get, post, put } from '../utils/request'

export default {
  login: data => post('/wx/customer/login', data),
  update: data => put('/wx/customer/update', data),
  info: () => get(`/wx/customer/info`)
}
