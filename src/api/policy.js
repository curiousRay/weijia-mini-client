import { get } from '../utils/request'

export default {
  get: () => get('/upload/policy')
}
