import { get } from '../utils/request'

export default {
  all: data => get(`/wx/diy/`, data)
}
