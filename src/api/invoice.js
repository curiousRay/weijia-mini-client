import { post } from '../utils/request'

export default {
  create: data => post(`/wx/invoiceInformation/`, data)
}
