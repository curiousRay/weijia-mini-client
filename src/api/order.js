import { post, put } from '../utils/request'

export default {
  create: data => post(`/wx/order/create`, data),
  preOrder: data => post(`/wx/order/preOrder`, data),
  detail: data => post(`/wx/order/detail`, data),
  my: data => post(`/wx/order/orders`, data),
  payInfo: orderCode => post(`/wx/order/payInfo?orderCode=${orderCode}`),
  cancel: orderCode => put(`/wx/order/cancel?orderCode=${orderCode}`),
  confirmReceipt: orderCode => put(`/wx/order/${orderCode}/confirmReceipt`)
}
