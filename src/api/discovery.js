import { get } from '../utils/request'

export default {
  all: data => get(`/wx/discovery/`, data),
  get: id => get(`/wx/discovery/${id}`)
}
