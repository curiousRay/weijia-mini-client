import { post, get } from '../utils/request'

export default {
  getList: data => get(`/wx/designmatch/all`, data),
  post: data => post(`/wx/designmatch/add`, data),
  getOne: id => get(`/wx/designmatch/${id}`)
}
