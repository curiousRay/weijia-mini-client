import { get, post } from '../utils/request'

export default {
  get: commodityCode => get(`/wx/evaluation/commodity/${commodityCode}`),
  // {
  //   "childCode": "string",
  //   "experience": "string",
  //   "pictureUrlList": [
  //     "string"
  //   ],
  //   "score": 0
  // }
  create: data => post(`/wx/evaluation/`, data),
  getCustomerList: data => get(`/wx/evaluation/customer`, data),
  rate: commodityCode => get(`/wx/evaluation/commodity/${commodityCode}/feedback`)
}
