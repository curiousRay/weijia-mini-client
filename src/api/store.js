import { get } from '../utils/request'

export default {
  flagshipStore: () => get('/wx/store/flagshipStore'),
  nearbyStore: data => get('/wx/store/nearbyStore', data),
  storeHome: storeCode => get(`/wx/store/${storeCode}/storeHome`),
  show: storeCode => get(`/wx/store/${storeCode}/show`)
}
