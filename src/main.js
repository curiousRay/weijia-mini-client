import Vue from 'vue'
import App from './App'
import store from './store'
import api from './api'
import clone from 'clone'

Vue.mixin({
  onLoad () {
    if (this.initData) {
      Object.assign(this, clone(this.initData))
    } else {
      this.initData = clone(this.$data)
    }

    this.$query = this.$root.$mp.query
  },
  watch: {
    '$root.$mp.query' (val) {
      this.$query = val
    }
  }
})

Vue.prototype.$store = store
Vue.prototype.$api = api
Vue.prototype.$bus = new Vue()

Vue.config.errorHandler = function (err, vm, info) {
  console.log(err, vm, info)
}

Vue.config.productionTip = false
App.mpType = 'app'

const app = new Vue(App)
app.$mount()

export default {
  // 这个字段走 app.json
  config: {
    // 页面前带有 ^ 符号的，会被编译成首页，其他页面可以选填，我们会自动把 webpack entry 里面的入口页面加进去
    pages: ['^pages/loading/main'],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      color: '#000000',
      selectedColor: '#8C0018',
      list: [{
        pagePath: 'pages/index/main',
        iconPath: 'static/nav/icon-home.png',
        selectedIconPath: 'static/nav/icon-home-active.png',
        text: '味家'
      }, {
        pagePath: 'pages/class/main',
        iconPath: 'static/nav/icon-class.png',
        selectedIconPath: 'static/nav/icon-class-active.png',
        text: '分类'
      }, {
        pagePath: 'pages/allPeopleDesign/main',
        iconPath: 'static/nav/icon-brush.png',
        selectedIconPath: 'static/nav/icon-brush-active.png',
        text: '全民设计'
      }, {
        pagePath: 'pages/showRoom/main',
        iconPath: 'static/nav/icon-eye.png',
        selectedIconPath: 'static/nav/icon-eye-active.png',
        text: '样板间'
      }, {
        pagePath: 'pages/userCenter/main',
        iconPath: 'static/nav/icon-user.png',
        selectedIconPath: 'static/nav/icon-user-active.png',
        text: '我的'
      }]
    }
  }
}
