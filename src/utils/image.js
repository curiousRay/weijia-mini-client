export default {
  choose (options) {
    return new Promise((resolve, reject) => {
      wx.chooseImage(Object.assign({
        count: 9,
        sizeType: ['original', 'compressed'],
        sourceType: ['album', 'camera'],
        success: resolve,
        fail: reject
      }, options))
    })
  },
  preview (options) {
    return new Promise((resolve, reject) => {
      wx.previewImage({
        current: options.urls[0],
        urls: options.urls,
        success: resolve,
        fail: reject,
        ...options
      })
    })
  },
  previewOneImage (url) {
    return this.preview({
      current: url,
      urls: [url]
    })
  }
}
