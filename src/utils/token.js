import store from './store'

export function setToken (token) {
  store.set('token', token)
}

export function getToken () {
  return store.get('token')
}

export function clearToken () {
  store.remove('token')
}

export default {
  set: setToken,
  get: getToken,
  clear: clearToken
}
