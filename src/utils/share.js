import { append } from './navigate'

export function createShareConfig (title, params = {}, config) {
  return {
    title,
    path: append(`/pages/loading/main`, params),
    ...config
  }
}

export default function (title, params = {}, config) {
  return {
    onShareAppMessage () {
      return createShareConfig(title, params, config)
    }
  }
}
