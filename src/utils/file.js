import image from './image'
import uuid from './uuid'

let _uptoken = ''
const UP_LOAD_URL = 'https://up.qbox.me'
const IMAGE_URL = 'https://one2ten.1to10.cn'

function setToken (uptoken) {
  _uptoken = uptoken
}

function handleURL (key) {
  return `${IMAGE_URL}/${key}`
}

function uploadFile (options) {
  return new Promise((resolve, reject) => {
    wx.uploadFile(Object.assign({
      url: UP_LOAD_URL,
      filePath: '',
      name: 'file',
      formData: {
        token: _uptoken,
        key: uuid() + _uptoken
      },
      success: res => {
        resolve(handleURL(JSON.parse(res.data).key))
      },
      fail: res => reject(res)
    }, options))
  })
}

function uploadOneImage () {
  return image.choose({ count: 1 })
    .then(res => ({ filePath: res.tempFilePaths[0] }))
    .then(uploadFile)
}

export default {
  setToken,
  handleURL,
  uploadFile,
  uploadOneImage
}
