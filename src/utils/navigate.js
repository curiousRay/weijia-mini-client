export function append (url, params) {
  Object.keys(params).forEach((key, index) => {
    const type = index === 0 ? '?' : '&'

    url = `${url}${type}${key}=${params[key]}`
  })

  return url
}

export default {
  redirect (url, params = {}) {
    wx.redirectTo({
      url: append(url, params)
    })
  },
  to (url, params = {}) {
    wx.navigateTo({
      url: append(url, params)
    })
  },
  switch (url) {
    wx.switchTab({
      url
    })
  },
  back () {
    wx.navigateBack()
  },
  reLaunch (url) {
    wx.reLaunch({
      url
    })
  }
}
