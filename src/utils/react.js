export default {
  showLoading (title = '读取中...') {
    wx.showLoading({
      title,
      mask: true
    })
  },
  hideLoading () {
    wx.hideLoading()
  },
  showToast (config) {
    if (Object(config) !== config) {
      let title = config

      config = {
        title
      }
    }

    return wx.showToast({
      mask: true,
      icon: 'none',
      ...config
    })
  },
  hideToast () {
    wx.hideToast()
  },
  showModal (options) {
    wx.showModal({
      confirmColor: '#8c001b',
      ...options
    })
  },
  modal (options) {
    return new Promise((resolve, reject) => {
      wx.showModal({
        confirmColor: '#8c001b',
        success: resolve,
        fail: reject,
        ...options
      })
    })
  }
}
