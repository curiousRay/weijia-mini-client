import api from '../api'
import navigate from './navigate'
import react from './react'

export async function payAgain (orderCode, jump = true) {
  const { prePayInfo, order } = await api.order.payInfo(orderCode)

  wx.requestPayment({
    ...prePayInfo,
    success () {
      if (jump) {
        navigate.to('/pages/orderPaySuccess/main', order)
      }
    },
    fail () {
      if (jump) {
        navigate.to('/pages/orderDetail/main', order)
      }
    }
  })
}

export async function cancel (orderCode, jump = true) {
  const res = await react.modal({
    title: '确认取消？'
  })

  if (!res.confirm) return

  react.showLoading('取消中...')

  await api.order.cancel(orderCode)

  react.hideLoading()
  react.showToast('取消成功')

  if (jump) {
    navigate.to('/pages/orderDetail/main', {
      orderCode
    })
  }
}

export async function confirmReceipt (orderCode, jump = true) {
  const res = await react.modal({
    title: '确认收货？'
  })

  if (!res.confirm) return

  react.showLoading('收货中...')

  await api.order.confirmReceipt(orderCode)

  react.hideLoading()
  react.showToast('收货成功')

  if (jump) {
    navigate.to('/pages/orderDetail/main', {
      orderCode
    })
  }
}

export default {
  payAgain,
  cancel,
  confirmReceipt
}
