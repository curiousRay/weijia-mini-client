export default {
  get (key) {
    let value = wx.getStorageSync(key)

    try {
      value = JSON.parse(value)
    } catch (e) {}

    return value
  },
  set (key, value) {
    if (Object(value) === value) {
      value = JSON.stringify(value)
    }

    wx.setStorageSync(key, value)
  },
  clearAll () {
    wx.clearStorageSync()
  },
  remove (key) {
    wx.removeStorageSync(key)
  }
}
