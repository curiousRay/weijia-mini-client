import { getToken } from '../utils/token'
import react from '../utils/react'

// const API_URL = 'http://roshen.oicp.net:23299'
// export const API_URL = 'http://47.98.53.75:8080'
export const API_URL = 'https://store.vekoo88.com/api'

function request (url, data, config) {
  return new Promise((resolve, reject) => {
    wx.request({
      url: API_URL + url,
      data,
      dataType: 'json',
      header: {
        'X-Token': getToken()
      },
      success (res) {
        if (res.data.code === '1000') {
          resolve(res.data.data)
        } else {
          react.showToast({
            title: res.data.msg,
            icon: 'none'
          })
          reject(new Error(res.data.msg))
        }
      },
      error (e) {
        reject(e)
      },
      ...config
    })
  })
}

export function get (url, data, config) {
  return request(url, data, {
    ...config,
    method: 'GET'
  })
}

export function post (url, data, config) {
  return request(url, data, {
    ...config,
    method: 'POST'
  })
}

export function put (url, data, config) {
  return request(url, data, {
    ...config,
    method: 'PUT'
  })
}

export function del (url, data, config) {
  return request(url, data, {
    ...config,
    method: 'DELETE'
  })
}

export default {
  get,
  post,
  put,
  del
}
